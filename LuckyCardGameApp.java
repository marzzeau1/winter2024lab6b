import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args) {
		GameManager manager = new GameManager();
		System.out.println("WELCOME TO LUCKYCARDGAME");
		int totalPoints = 0;
		int i = 0;
		
		while(totalPoints < 5 && manager.getNumberOfCards() > 1){
			System.out.println("Round " + i);
			manager.dealCards();
			totalPoints += manager.calculatePoints();
			System.out.println(manager);
			System.out.println("Your points after round " + i + ":" + totalPoints);
			i++;
		}
	}
}