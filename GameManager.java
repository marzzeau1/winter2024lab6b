
public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
    
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public String toString(){
		return "----------------------------------------------------------\n Center Card:" + this.centerCard +  "\n Player Card:" + this.playerCard + "\n----------------------------------------------------------";
	}
	
	public void dealCards(){
		this.drawPile.shuffle();
		this.drawPile.drawTopCard();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return drawPile.length();
	}
	
	public int calculatePoints(){
		if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		}
		if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return 4;
		}
			return -1;
	}
}